import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import SessionList from "@/components/SessionList.vue";

const localVue = createLocalVue();
localVue.use(VueRouter);
const router = new VueRouter();

describe("Session list have to", () => {
  const wrapper = shallowMount(SessionList, {
    localVue,
    router
  });
  it("renders a list of sessions", () => {
    expect(wrapper.find("#sessionList").exists()).toBe(true);
    expect(wrapper.find("#sessionList").is("table")).toBe(true);
  });
  it("shows the name, date and status for each session", () => {
    expect(wrapper.find("#sessionList thead tr th#name").exists()).toBe(true);
    expect(wrapper.find("#sessionList thead tr th#date").exists()).toBe(true);
    expect(wrapper.find("#sessionList thead tr th#status").exists()).toBe(true);
  });
});
