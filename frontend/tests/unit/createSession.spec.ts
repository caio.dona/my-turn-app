import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import CreateSession from "@/views/CreateSession.vue";

const localVue = createLocalVue();
localVue.use(VueRouter);
const router = new VueRouter();

describe("Create session have to", () => {
  const wrapper = shallowMount(CreateSession, {
    localVue,
    router
  });
  it("renders name field", () => {
    expect(wrapper.find("#name").exists()).toBe(true);
    expect(wrapper.find("#name").is("input")).toBe(true);
  });
  it("renders date field", () => {
    expect(wrapper.find("#date").exists()).toBe(true);
    expect(wrapper.find("#date").is("input")).toBe(true);
  });
  it("renders a button with the text 'Save'", () => {
    const button = wrapper.find("#createSession");
    expect(button.is("button")).toBe(true);
    expect(button.text()).toBe("Save");
  });
});
