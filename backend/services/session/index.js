'use strict'

var sessions = [];

module.exports = function (fastify, opts, next) {
  fastify.get('/session', function (request, reply) {
    request.log.info('Get all session requested');
    reply
      .code(200)
      .type('application/json')
      .send(sessions);
  })

  fastify.post('/session', function (request, reply) {
    request.log.info('Create session requested');
    var sessionToCreate = request.body;
    request.log.trace('Session that will be created', sessionToCreate);
    sessionToCreate.status = "Created";
    sessions.push(sessionToCreate);
    reply
      .code(200)
      .type('application/json')
      .send(request.body);
  })

  fastify.put('/session/:id', function (request, reply) {
    request.log.info('Update session requested');
    var sessionUpdated = request.body;
    request.log.trace('Data to update', sessionUpdated);
    var sessionRequested = sessions.filter(function(session) {
      return session.id = request.params.id;
    })
    request.log.trace('Session found by ID', sessionRequested);
    if (sessionRequested.length > 0) {
      var sessionToUpdate = sessionRequested[0];
      var sessionReady = Object.assign(sessionToUpdate, sessionUpdated);
      request.log.trace('Session already merged', sessionReady);
      sessions = sessions.map(function(session) {
        if (session.id == sessionReady.id) {
          return sessionReady;
        }
        return session;
      });
      request.log.debug('Session updated');
      reply
        .code(200)
        .type('application/json')
        .send(sessionReady);
    } else {
      reply
        .code(404)
        .send('Session not found');
    }
  })

  fastify.get('/session/:id', function (request, reply) {
    request.log.info('Get session by ID requested');
    var sessionRequested = sessions.filter(function(session) {
      return session.id = request.params.id;
    });
    request.log.trace('Session found by ID', sessionRequested);
    if (sessionRequested.length > 0) {
      reply
        .code(200)
        .type('application/json')
        .send(sessionRequested[0]);
    } else {
      reply
        .code(404)
        .send('Session not found');
    }
  })

  fastify.delete('/session/:id', function (request, reply) {
    request.log.info('Delete session by ID requested');
    request.log.debug('Delete session by ID', request.params.id);
    var quantityBefore = sessions.length;
    sessions = sessions.filter(function(session) {
      return session.id != request.params.id;
    })
    request.log.trace('Sessions without ID deleted', sessions);
    if (quantityBefore > sessions.length) {
      reply
        .code(204)
        .send("Session deleted");
    } else {
      reply
        .code(404)
        .send('Session not found');
    }
  })

  next()
}

// If you prefer async/await, use the following
//
// module.exports = async function (fastify, opts) {
//   fastify.get('/example', async function (request, reply) {
//     return 'this is an example'
//   })
// }
