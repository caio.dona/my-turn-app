import { shallowMount, createLocalVue } from "@vue/test-utils";
import VueRouter from "vue-router";
import SessionDetails from "@/views/SessionDetails.vue";

const localVue = createLocalVue();
localVue.use(VueRouter);
const router = new VueRouter();

describe("Session details have to", () => {
  const wrapper = shallowMount(SessionDetails, {
    localVue,
    router
  });
  it("renders name field", () => {
    expect(wrapper.find("#name").exists()).toBe(true);
    expect(wrapper.find("#name").is("input")).toBe(true);
  });
  it("renders date field", () => {
    expect(wrapper.find("#date").exists()).toBe(true);
    expect(wrapper.find("#date").is("input")).toBe(true);
  });
  it("renders a button with the text 'Save'", () => {
    const button = wrapper.find("#updateSession");
    expect(button.is("button")).toBe(true);
    expect(button.text()).toBe("Save");
  });
  it("renders a button with the text 'Delete'", () => {
    const button = wrapper.find("#deleteSession");
    expect(button.is("button")).toBe(true);
    expect(button.text()).toBe("Delete");
  });
});
