# My Turn App

A Platform to take turns to speak or share something during a collaborative session, it's a good tool for remote and presential sessions.

## Frontend

Made with Vuejs framework, the project is in the folder [frontend](./frontend).

## Backend

Made with Fastify framework, the project is in the folder [backend](./backend).

## Deploy on Openshift

To deploy this app on Openshift platform follow this [guide](./openshift).
