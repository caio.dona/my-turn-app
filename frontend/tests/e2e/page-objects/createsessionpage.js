/**
 * A Nightwatch page object. The page object name is the filename.
 *
 * Example usage:
 *   browser.page.homepage.navigate()
 *
 * For more information on working with page objects see:
 *   https://nightwatchjs.org/guide/working-with-page-objects/
 *
 */

module.exports = {
  url: "/create",
  commands: [],
  elements: {
    appContainer: "#create-session"
  },
  sections: {
    app: {
      selector: "#create-session",
      elements: {
        title: "#header-title"
      },
      sections: {
        createform: {
          selector: "form",
          elements: {
            nameField: "input[id=name]",
            dateField: "input[id=date]",
            submitButton: "button[id=createSession]"
          }
        }
      }
    }
  }
};
