'use strict'

const { test } = require('tap')
const { build } = require('../helper')

test('Create a session', (t) => {
  t.plan(2)
  const app = build(t)

  app.inject({
    method: 'POST',
    url: '/api/session',
    payload: {
      id: '123',
      name: 'Session 1',
      date: '23/04/2020'
    }
  }, (err, res) => {
    t.error(err);
    t.deepEqual(JSON.parse(res.payload), {
      id: '123',
      name: 'Session 1',
      date: '23/04/2020',
      status: 'Created'
    });
  })
})

test('Get all created sessions', (t) => {
  t.plan(2)
  const app = build(t)

  app.inject({
    url: '/api/session'
  }, (err, res) => {
    t.error(err);
    t.deepEqual(JSON.parse(res.payload), [{
      id: '123',
      name: 'Session 1',
      date: '23/04/2020',
      status: 'Created'
    }]);
  })
})

test('Update a session by ID', (t) => {
  t.plan(2)
  const app = build(t)

  app.inject({
    method: 'PUT',
    url: '/api/session/123',
    payload: {
      name: 'Session 1 updated',
      date: '24/04/2020',
      status: 'Started'
    }
  }, (err, res) => {
    t.error(err);
    t.deepEqual(JSON.parse(res.payload), {
      id: '123',
      name: 'Session 1 updated',
      date: '24/04/2020',
      status: 'Started'
    });
  })
})

test('Get a session by ID', (t) => {
  t.plan(2)
  const app = build(t)

  app.inject({
    method: 'GET',
    url: '/api/session/123'
  }, (err, res) => {
    t.error(err);
    t.deepEqual(JSON.parse(res.payload), {
      id: '123',
      name: 'Session 1 updated',
      date: '24/04/2020',
      status: 'Started'
    });
  })
})

test('Delete a session', (t) => {
  t.plan(2)
  const app = build(t)

  app.inject({
    method: 'DELETE',
    url: '/api/session/123'
  }, (err, res) => {
    t.error(err);
    t.equal(res.statusCode, 204);
  })
})

// If you prefer async/await, use the following
//
// test('example is loaded', async (t) => {
//   const app = build(t)
//
//   const res = await app.inject({
//     url: '/example'
//   })
//   t.equal(res.payload, 'this is an example')
// })
