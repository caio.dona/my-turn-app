interface Session {
  id: string;
  name: string;
  date: string;
  status: string;
}

export default Session;
