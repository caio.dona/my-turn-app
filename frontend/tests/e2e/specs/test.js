// For authoring Nightwatch tests, see
// https://nightwatchjs.org/guide

module.exports = {
  "Create a session going to the form after to click in the Create Session button in the Home, once the session was created it have to be in a list in the Home": browser => {
    browser
      .init()
      .waitForElementVisible("#session-list")
      .click("a[id=createSession]")
      .waitForElementVisible("#create-session")
      .assert.visible("input[id=name]")
      .setValue("input[id=name]", "Session 1")
      .assert.visible("input[id=date]")
      .setValue("input[id=date]", "23/04/2020")
      .click("button[id=createSession]")
      .waitForElementVisible(".pf-c-modal-box")
      .assert.containsText("#modal-description", "Your session was registered.")
      .click("a[type=button]")
      .waitForElementVisible("#home")
      .assert.visible("table#sessionList tbody tr th[data-label=Name]")
      .assert.containsText(
        "table#sessionList tbody tr th[data-label=Name]",
        "Session 1"
      )
      .assert.containsText(
        "table#sessionList tbody tr td[data-label=Date]",
        "23/04/2020"
      )
      .assert.containsText(
        "table#sessionList tbody tr td[data-label=Status]",
        "Created"
      )
      .end();
  },
  "Select a session in the list to update its name and date": browser => {
    browser
      .init()
      .waitForElementVisible("#session-list")
      .click("table#sessionList tbody tr th[data-label=Name] div a")
      .waitForElementVisible("#session-details")
      .assert.visible("input[id=name]")
      .setValue("input[id=name]", "Session 1 updated")
      .assert.visible("input[id=date]")
      .setValue("input[id=date]", "24/05/2021")
      .click("button[id=updateSession]")
      .waitForElementVisible(".pf-c-modal-box")
      .assert.containsText("#modal-description", "Your session was updated.")
      .click("a[id=ok-button]")
      .waitForElementVisible("#home")
      .assert.visible("table#sessionList tbody tr th[data-label=Name]")
      .assert.containsText(
        "table#sessionList tbody tr th[data-label=Name]",
        "Session 1 updated"
      )
      .assert.containsText(
        "table#sessionList tbody tr td[data-label=Date]",
        "24/05/2021"
      )
      .assert.containsText(
        "table#sessionList tbody tr td[data-label=Status]",
        "Created"
      )
      .end();
  },
  "Select a session in the list to delete it": browser => {
    browser
      .init()
      .waitForElementVisible("#session-list")
      .click("table#sessionList tbody tr th[data-label=Name] div a")
      .waitForElementVisible("#session-details")
      .click("button[id=deleteSession]")
      .waitForElementVisible(".pf-c-modal-box")
      .assert.containsText(
        "#modal-description",
        "Are you sure to delete this session?"
      )
      .click("button[id=startDeletion]")
      .waitForElementVisible("a[id=ok-button]")
      .assert.containsText("#modal-description", "The session was deleted.")
      .click("a[id=ok-button]")
      .waitForElementVisible("#home")
      // .assert.not.containsText("table#sessionList tbody tr th[data-label=Name]", "Session 1 updated")
      .end();
  }
};
