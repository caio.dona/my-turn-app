import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../views/Home.vue";
import CreateSession from "../views/CreateSession.vue";
import SessionDetails from "../views/SessionDetails.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: "My Turn App - Home"
    }
  },
  {
    path: "/create",
    name: "Create Session",
    component: CreateSession,
    meta: {
      title: "My Turn App - Create Session"
    }
  },
  {
    path: "/details/:id",
    name: "Session Details",
    component: SessionDetails,
    meta: {
      title: "My Turn App - Session Details"
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
