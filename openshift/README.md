# My Turn App - Deploy on Openshift

To deploy this app on Openshift follow th next steps:

1. Clone this repository:

    ```bash
    $ git clone https://gitlab.com/caio.dona/my-turn-app.git
    ````

2. Go to `openshift` folder:

    ```bash
    $ cd my-turn-app/openshift
    ```

3. Login to th Openshift cluster where you going to deploy and create a project called **my-turn-app**:

    ```bash
    $ oc login <CLUSTER_URL>
    $ oc new-project my-turn-app
    ```

4. Create the builds, deployments and networking components:

    ```bash
    $ oc create -f ./
    ```

5. Build the frontend layer to generate the web artefact:

    ```bash
    $ cd ../frontend
    $ npm install
    $ export PROTOCOLO=`if [ "$(oc get route my-turn-app-frontend -o jsonpath='{ .spec.tls.termination }')" != "" ]; then echo "https"; else echo "http"; fi`
    $ echo "VUE_APP_API_URL=$PROTOCOLO://$(oc get route my-turn-app-frontend -o jsonpath='{ .spec.host }')" | tee .env.local
    $ npm run build
    ```

6. Build the container image using the web artefact:

    ```bash
    $ oc start-build my-turn-app-frontend --from-dir ./dist --follow
    ```

7. Check if the app is working using the e2e test:

    ```bash
    $ npm run test:e2e-remote "$(oc get route my-turn-app-frontend -o jsonpath='{ .spec.host }')"
    ```
